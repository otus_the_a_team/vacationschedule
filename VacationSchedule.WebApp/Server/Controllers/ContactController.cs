﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public ContactController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<UserContact> Get()
        {
            return _db.UserContacts;
        }

        // GET api/<ValuesController>/5
        [HttpGet("GetContactsByUserId")]
        public IEnumerable<UserContact>? GetContactsByUserId(int id)
        {
            var user = _db.Users.Where(x => x.Id == id).FirstOrDefault();
            if (user is User)
            {
                return _db.UserContacts
                    .Where(x => x.UserId == user.Id)
                    .Include(x => x.TypeContact);
            }
            return new List<UserContact>();
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] UserContact value)
        {
            if (value.Id == 0)
            {
                _db.UserContacts.Add(value);
                _db.SaveChanges();
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UserContact value)
        {
            var existContact = _db.UserContacts.Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            if (existContact is UserContact)
            {
                _db.UserContacts.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var contact = _db.UserContacts.Where(x => x.Id == id).FirstOrDefault();
            if (contact is UserContact)
            {
                _db.UserContacts.Remove(contact);
                _db.SaveChanges();
            }
        }
    }
}
