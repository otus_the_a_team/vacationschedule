﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactTypeController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public ContactTypeController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<ContactType> Get()
        {
            return _db.ContactTypes;
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public IEnumerable<ContactType>? Get(int id)
        {
            return _db.ContactTypes.Where(x => x.Id == id);
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] ContactType value)
        {
            if (value.Id == 0)
            {
                _db.ContactTypes.Add(value);
                _db.SaveChanges();
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ContactType value)
        {
            var existContactType = _db.ContactTypes.Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            if (existContactType is ContactType)
            {
                _db.ContactTypes.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
