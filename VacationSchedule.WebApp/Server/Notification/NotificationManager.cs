﻿using Microsoft.EntityFrameworkCore;
using VacationSchedule.DBEntity;
using VacationSchedule.WebApp.Server.RabbitMQProducers;

namespace VacationSchedule.WebApp.Server.Notification
{
    public class NotificationManager
    {
        private readonly PostgreBdContext _db;
        private readonly ISendEmailRabbitProducer _sendEmailRabbitMQProducer;
        private string _defaultNotificationType = "Email";
        public NotificationManager(PostgreBdContext db, ISendEmailRabbitProducer sendEmailRabbitMQProducer)
        {
            _db = db;
            _sendEmailRabbitMQProducer = sendEmailRabbitMQProducer;
        }
        public void Send()
        {
            var subject = "Система планирования отпусков - Уведомление";
            var message = "через 2 недели запланированные выходные";
            var messageHOD = "Сотрудники которые уходят в отпуск: ";
            var contactTypes = _db.ContactTypes.ToList();

            //пользователи, которые уже уведомлены
            List<Person> users = new List<Person>();
            //главы департаментов для уведомления
            Dictionary<string,List<string>> headDeparts = new Dictionary<string,List<string>>();

            List<Task> querys = new();
            //собираем пользователей с датой начала выходного через 2 недели
            foreach (var c in contactTypes)
            {
                var contact = _db.Users
                    .Include(x => x.UserContacts)
                    .Include(x => x.Vacations)
                    .Include(x => x.HeadDepartment)
                    .Where(x => x.Vacations.Any(p => p.VacationStartDate == new DateTime(2023,02,07)//DateTime.Now.AddDays(14).Date
                        && p.IsAccepted == true
                        && x.UserContacts.Any(p => p.TypeContactId == c.Id)
                        ))
                    .Select(x => new Person
                    {
                        Id = x.Id,
                        Name = x.FirstName + " " + x.LastName,
                        address = x.UserContacts.FirstOrDefault(p => p.TypeContactId == c.Id).Contact,
                        depId = x.DepartmentId,
                        isHeadOfDepartment = x.HeadDepartmentId != null ? true : false
                    })
                    .ToList();

                var userForNotification = contact.Where(x => !users.Select(z=>z.Id).Contains(x.Id)).ToList();

                if (userForNotification.Count != 0)
                {
                    users.AddRange(userForNotification.Select(x => new Person { Id = x.Id, Name = x.Name, depId = x.depId, isHeadOfDepartment = x.isHeadOfDepartment }));
                    querys.Add(Task.Run(() => ChooseNotificationManager(c.Name).Send(userForNotification.Select(x => x.address), subject, message)));
                }
            }
            foreach (var h in SendHeadOfDep(users.Where(x=> !x.isHeadOfDepartment)))
            {
                var employeesToVacation = String.Join(",", users.Where(x => x.depId == h.depId).Select(x => x.Name));

                querys.Add(Task.Run(() =>
                    ChooseNotificationManager(_defaultNotificationType)
                        .Send(
                            new List<string> { h.address },
                            subject,
                            messageHOD + employeesToVacation
                        )
                    )
                );
            }
            users.Clear();
            Task.WaitAll(querys.ToArray());
        }

        private IEnumerable<Person> SendHeadOfDep(IEnumerable<Person> people)
        {
            return _db.Departments
                .Include(x => x.HeadOfDepartmentUser)
                .ThenInclude(x=>x.UserContacts)
                .Where(x => people.Select(z => z.depId).Distinct().Contains(x.Id))
                .Select(x=> new Person
                    {
                        Id = x.HeadOfDepartmentUser.Id,
                        address = x.HeadOfDepartmentUser
                                    .UserContacts
                                    .FirstOrDefault(p => p.TypeContact.Name == _defaultNotificationType).Contact,
                        depId = x.Id
                    })
                .ToList();
        }

        private INotification ChooseNotificationManager(string _type)
        {
            switch (_type.ToLower())
            {
                case "email":
                    return new NotificationMail(_sendEmailRabbitMQProducer);
                case "telegram":
                    return new NotificationTelegram();
                case "phone":
                    return new NotificationPhone();
                default: return null;
            }
        }

        private struct Person
        {
            public int Id;
            public string Name;
            public string address;
            public int? depId;
            public bool isHeadOfDepartment;
        }
    }
}
