﻿namespace VacationSchedule.Core.Models.DB
{
    public class Vacation
    {
        public int Id { get; set; }
        public int UserId { get; set; }                         // внешний ключ
        public User? User { get; set; }                         // навигационное свойство на таблицу User для EFCore
        public DateTime VacationStartDate { get; set; }
        public DateTime VacationEndDate { get; set; }
        public int VacationTypeId { get; set; }                 // внешний ключ
        public VacationType? VacationType { get; set; }         // навигационное свойство на таблицу VocationType для EFCore

        //public int ReplacementUserId { get; set; }              // внешний ключ
        //public User? ReplacementUser { get; set; }              // навигационное свойство на таблицу User для EFCore
        public bool IsAccepted { get; set; }
        public string? Comment { get; set; }
    }
}
