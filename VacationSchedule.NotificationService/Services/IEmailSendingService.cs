﻿using VacationSchedule.NotificationService.Model;

namespace VacationSchedule.NotificationService.Services
{
    public interface IEmailSendingService
    {
        public Task SendEmailAsync(SendEmailRequest emailRequest);
    }
}
