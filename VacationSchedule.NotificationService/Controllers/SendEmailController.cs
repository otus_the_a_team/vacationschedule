using Microsoft.AspNetCore.Mvc;
using VacationSchedule.NotificationService.Model;
using VacationSchedule.NotificationService.Services;

namespace VacationSchedule.NotificationService.Controllers
{
    /// <summary>
    /// Control for email sending
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class SendEmailController : ControllerBase
    {
        private readonly IEmailSendingService _emailSendingService;
        private Serilog.ILogger _logger;

        public SendEmailController(IEmailSendingService emailSendingService, Serilog.ILogger logger)
        {
            _emailSendingService = emailSendingService;
            _logger = logger;
        }

        /// <summary>
        /// Send email
        /// </summary>
        [HttpPost("SendEmail")]
        public async Task<IActionResult> SendEmail(SendEmailRequest request)
        {
            try
            {
                await _emailSendingService.SendEmailAsync(request);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, $"Email sending error");
            }

        }
    }
}
