﻿using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;

namespace VacationSchedule.DBEntity
{
    public class PostgreBdContext : DbContext
    {
        public DbSet<Role>? Role { get; set; }
        public DbSet<User>? Users { get; set; }
        public DbSet<Department>? Departments { get; set; }
        public DbSet<Vacation> Vacations { get; set; }
        public DbSet<VacationType> VacationTypes { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<UserContact> UserContacts { get; set; }
        public DbSet<ContactType> ContactTypes { get; set; }


        public PostgreBdContext(DbContextOptions<PostgreBdContext> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasKey(x => x.Id);
            modelBuilder.Entity<User>(e =>
                {
                    e.HasKey(x => x.Id);
                    e.HasOne(x => x.Department)
                        .WithMany(x => x.Users)
                        .HasForeignKey(x => x.DepartmentId)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Restrict);
                    e.HasOne(x => x.HeadDepartment)
                        .WithOne(x => x.HeadOfDepartmentUser)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Restrict);
                }
            );
            modelBuilder.Entity<Department>(e =>
                {
                    e.HasKey(x => x.Id);
                    e.HasOne(x => x.Parent)
                        .WithMany(x => x.SubDepartments)
                        .HasForeignKey(x => x.ParentId)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Restrict);
                    e.HasOne(x => x.HeadOfDepartmentUser)
                        .WithOne(x => x.HeadDepartment)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Restrict);
                }
            );
            modelBuilder.Entity<Vacation>().HasKey(x => x.Id);
            modelBuilder.Entity<VacationType>().HasKey(x => x.Id);
            modelBuilder.Entity<Log>().HasKey(x => x.Id);
            modelBuilder.Entity<UserContact>(e =>
            {
                e.HasKey(x => x.Id);
                e.HasOne(x => x.User)
                    .WithMany(x => x.UserContacts)
                    .HasForeignKey(x => x.UserId)
                    .IsRequired(true)
                    .OnDelete(DeleteBehavior.Restrict);
                e.HasOne(x => x.TypeContact)
                    .WithMany(x => x.Contacts)
                    .HasForeignKey(x => x.TypeContactId)
                    .IsRequired(true)
                    .OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<ContactType>().HasKey(x => x.Id);

            modelBuilder.Entity<Role>().HasData(new Role[] {
                new Role { Id = 1, RoleName = "Admin",     IsAdminRights = true,  IsHeadOfDepartmentRights = false },
                new Role { Id = 2, RoleName = "Moderator", IsAdminRights = false, IsHeadOfDepartmentRights = false },
                new Role { Id = 3, RoleName = "Basic",     IsAdminRights = false, IsHeadOfDepartmentRights = false },
                new Role { Id = 4, RoleName = "Unknown",   IsAdminRights = false, IsHeadOfDepartmentRights = false }
            });

            modelBuilder.Entity<User>().HasData(new User[] {
                new User { Id = 1,  Login = "admin", LastName ="Дементьева", FirstName = "Марьям",   PatronymicName = "Ивановна",       Position = "Администратор",         RoleId = 1, DepartmentId = 2, HeadDepartmentId = 2},
                new User { Id = 2,  Login = "hr1",   LastName ="Худяков",    FirstName = "Егор",     PatronymicName = "Владимирович",   Position = "hr1",                   RoleId = 2, DepartmentId = 3, HeadDepartmentId = 3},
                new User { Id = 3,  Login = "hr2",   LastName ="Фомин",      FirstName = "Руслан",   PatronymicName = "Кириллович",     Position = "hr2",                   RoleId = 2, DepartmentId = 3,},
                new User { Id = 4,  Login = "dev1",  LastName ="Ефимов",     FirstName = "Евгений",  PatronymicName = "Андреевич",      Position = "разработчик1",          RoleId = 3, DepartmentId = 2,},
                new User { Id = 5,  Login = "dev2",  LastName ="Федорова",   FirstName = "Виктория", PatronymicName = "Александровна",  Position = "Руководитель отдела",   RoleId = 3, DepartmentId = 5, HeadDepartmentId = 5},
                new User { Id = 6,  Login = "dev3",  LastName ="Лебедев",    FirstName = "Денис",    PatronymicName = "Иванович",       Position = "разработчик2",          RoleId = 3, DepartmentId = 5,},
                new User { Id = 7,  Login = "user1", LastName ="Короткова",  FirstName = "Анастасия",PatronymicName = "Константиновна", Position = "Руководитель отдела",   RoleId = 3, DepartmentId = 4, HeadDepartmentId = 4},
                new User { Id = 8,  Login = "user2", LastName ="Калинин",    FirstName = "Владимир", PatronymicName = "Адамович",       Position = "Инженер-логист",        RoleId = 3, DepartmentId = 4,},
                new User { Id = 9,  Login = "user3", LastName ="Рыбакова",   FirstName = "Валерия",  PatronymicName = "Филипповна",     Position = "Инженер-логист",        RoleId = 3, DepartmentId = 4,},
                new User { Id = 10, Login = "user4", LastName ="Зайцева",    FirstName = "Василиса", PatronymicName = "Дмитриевна",     Position = "Инженер-логист",        RoleId = 3, DepartmentId = 4,},
            });

            modelBuilder.Entity<Department>().HasData(new Department[] {
                new Department { Id = 1, DepartmentName = "Неизвестно" },
                new Department { Id = 2, DepartmentName = "ИТ" },
                new Department { Id = 3, DepartmentName = "HR" },
                new Department { Id = 4, DepartmentName = "Логистика" },
                new Department { Id = 5, DepartmentName = "Отдел разработки",   ParentId = 2 },
            });

            modelBuilder.Entity<VacationType>().HasData(new VacationType[] {
                new VacationType { Id = 1, Name = "Отпуск"},
                new VacationType { Id = 2, Name = "Отгул"},
                new VacationType { Id = 3, Name = "Отпуск без содержания" }
            });

            modelBuilder.Entity<ContactType>().HasData(new ContactType[] {
                new ContactType { Id = 1, Name = "Telegram" },
                new ContactType { Id = 2, Name = "Email" },
                new ContactType { Id = 3, Name = "Phone" },
            });

            modelBuilder.Entity<UserContact>().HasData(new UserContact[] {
                new UserContact { Id = 1,  TypeContactId = 2, UserId = 1,  Contact = "admin@mail.ru"},
                new UserContact { Id = 2,  TypeContactId = 2, UserId = 2,  Contact = "hr1@mail.ru"  },
                new UserContact { Id = 3,  TypeContactId = 2, UserId = 3,  Contact = "hr2@mail.ru"  },
                new UserContact { Id = 4,  TypeContactId = 2, UserId = 4,  Contact = "dev1@mail.ru" },
                new UserContact { Id = 5,  TypeContactId = 2, UserId = 5,  Contact = "dev2@mail.ru" },
                new UserContact { Id = 6,  TypeContactId = 2, UserId = 6,  Contact = "dev3@mail.ru" },
                new UserContact { Id = 7,  TypeContactId = 2, UserId = 7,  Contact = "user1@mail.ru"},
                new UserContact { Id = 8,  TypeContactId = 2, UserId = 8,  Contact = "user2@mail.ru"},
                new UserContact { Id = 9,  TypeContactId = 2, UserId = 9,  Contact = "user3@mail.ru"},
                new UserContact { Id = 10, TypeContactId = 2, UserId = 10, Contact = "user4@mail.ru"},
                new UserContact { Id = 12, TypeContactId = 1, UserId = 4,  Contact = "@dev1"},
                new UserContact { Id = 13, TypeContactId = 1, UserId = 5,  Contact = "@dev2"},
                new UserContact { Id = 14, TypeContactId = 1, UserId = 6,  Contact = "@dev3"},
            });

            modelBuilder.Entity<Vacation>().HasData(new Vacation[] {
                new Vacation { Id = 1,  UserId = 1,  VacationStartDate = new DateTime(2023,02,07, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,02,20, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 2,  UserId = 1,  VacationStartDate = new DateTime(2023,07,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,07,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 3,  UserId = 2,  VacationStartDate = new DateTime(2023,04,11, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,04,28, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 4,  UserId = 2,  VacationStartDate = new DateTime(2023,06,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,06,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 5,  UserId = 3,  VacationStartDate = new DateTime(2023,07,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,07,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 6,  UserId = 3,  VacationStartDate = new DateTime(2023,10,15, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,10,28, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 7,  UserId = 4,  VacationStartDate = new DateTime(2023,08,05, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,08,19, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 8,  UserId = 4,  VacationStartDate = new DateTime(2023,11,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,11,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 9,  UserId = 5,  VacationStartDate = new DateTime(2023,04,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,04,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 10, UserId = 5,  VacationStartDate = new DateTime(2023,07,11, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,07,28, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 11, UserId = 6,  VacationStartDate = new DateTime(2023,02,07, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,02,20, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 12, UserId = 6,  VacationStartDate = new DateTime(2023,07,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,07,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 13, UserId = 7,  VacationStartDate = new DateTime(2023,01,15, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,01,29, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 14, UserId = 7,  VacationStartDate = new DateTime(2023,08,05, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,08,19, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 15, UserId = 8,  VacationStartDate = new DateTime(2023,02,07, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,02,20, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 16, UserId = 8,  VacationStartDate = new DateTime(2023,07,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,07,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 17, UserId = 9,  VacationStartDate = new DateTime(2023,04,11, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,04,24, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 18, UserId = 9,  VacationStartDate = new DateTime(2023,06,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,06,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 19, UserId = 10, VacationStartDate = new DateTime(2023,09,01, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,09,14, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
                new Vacation { Id = 20, UserId = 10, VacationStartDate = new DateTime(2023,12,15, 0,0,0, DateTimeKind.Local), VacationEndDate = new DateTime(2023,12,29, 0,0,0, DateTimeKind.Local), VacationTypeId = 1, IsAccepted = true},
            });

            modelBuilder.Entity<Log>().HasData(new Log[] {
                new Log { Id = 1, LogMessage = "Первая запись", LogDateTime = new DateTime(2022,02,01, 0,0,0, DateTimeKind.Local) },
                new Log { Id = 2, LogMessage = "Вторая запись", LogDateTime = new DateTime(2022,02,01, 0,0,0, DateTimeKind.Local) }
            });
        }
    }
}