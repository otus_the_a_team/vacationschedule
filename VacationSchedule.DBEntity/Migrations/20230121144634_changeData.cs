﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace VacationSchedule.DBEntity.Migrations
{
    public partial class changeData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ContactTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DepartmentName = table.Column<string>(type: "text", nullable: true),
                    ParentId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_Departments_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LogDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LogMessage = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleName = table.Column<string>(type: "text", nullable: true),
                    IsAdminRights = table.Column<bool>(type: "boolean", nullable: false),
                    IsHeadOfDepartmentRights = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VacationTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VacationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Login = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    PatronymicName = table.Column<string>(type: "text", nullable: true),
                    Position = table.Column<string>(type: "text", nullable: true),
                    RoleId = table.Column<int>(type: "integer", nullable: true),
                    DepartmentId = table.Column<int>(type: "integer", nullable: true),
                    HeadDepartmentId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Departments_HeadDepartmentId",
                        column: x => x.HeadDepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "UserContacts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Contact = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    TypeContactId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserContacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserContacts_ContactTypes_TypeContactId",
                        column: x => x.TypeContactId,
                        principalTable: "ContactTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserContacts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vacations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    VacationStartDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    VacationEndDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    VacationTypeId = table.Column<int>(type: "integer", nullable: false),
                    IsAccepted = table.Column<bool>(type: "boolean", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vacations_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vacations_VacationTypes_VacationTypeId",
                        column: x => x.VacationTypeId,
                        principalTable: "VacationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ContactTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Telegram" },
                    { 2, "Email" },
                    { 3, "Phone" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "DepartmentName", "ParentId" },
                values: new object[,]
                {
                    { 1, "Неизвестно", null },
                    { 2, "ИТ", null },
                    { 3, "HR", null },
                    { 4, "Логистика", null }
                });

            migrationBuilder.InsertData(
                table: "Logs",
                columns: new[] { "Id", "LogDateTime", "LogMessage" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 2, 1, 0, 0, 0, 0, DateTimeKind.Local), "Первая запись" },
                    { 2, new DateTime(2022, 2, 1, 0, 0, 0, 0, DateTimeKind.Local), "Вторая запись" }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "IsAdminRights", "IsHeadOfDepartmentRights", "RoleName" },
                values: new object[,]
                {
                    { 1, true, false, "Admin" },
                    { 2, false, false, "Moderator" },
                    { 3, false, false, "Basic" },
                    { 4, false, false, "Unknown" }
                });

            migrationBuilder.InsertData(
                table: "VacationTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Отпуск" },
                    { 2, "Отгул" },
                    { 3, "Отпуск без содержания" }
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "DepartmentName", "ParentId" },
                values: new object[] { 5, "Отдел разработки", 2 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DepartmentId", "FirstName", "HeadDepartmentId", "LastName", "Login", "Password", "PatronymicName", "Position", "RoleId" },
                values: new object[,]
                {
                    { 1, 2, "Марьям", 2, "Дементьева", "admin", null, "Ивановна", "Администратор", 1 },
                    { 2, 3, "Егор", 3, "Худяков", "hr1", null, "Владимирович", "hr1", 2 },
                    { 3, 3, "Руслан", null, "Фомин", "hr2", null, "Кириллович", "hr2", 2 },
                    { 4, 2, "Евгений", null, "Ефимов", "dev1", null, "Андреевич", "разработчик1", 3 },
                    { 7, 4, "Анастасия", 4, "Короткова", "user1", null, "Константиновна", "Руководитель отдела", 3 },
                    { 8, 4, "Владимир", null, "Калинин", "user2", null, "Адамович", "Инженер-логист", 3 },
                    { 9, 4, "Валерия", null, "Рыбакова", "user3", null, "Филипповна", "Инженер-логист", 3 },
                    { 10, 4, "Василиса", null, "Зайцева", "user4", null, "Дмитриевна", "Инженер-логист", 3 }
                });

            migrationBuilder.InsertData(
                table: "UserContacts",
                columns: new[] { "Id", "Contact", "TypeContactId", "UserId" },
                values: new object[,]
                {
                    { 1, "admin@mail.ru", 2, 1 },
                    { 2, "hr1@mail.ru", 2, 2 },
                    { 3, "hr2@mail.ru", 2, 3 },
                    { 4, "dev1@mail.ru", 2, 4 },
                    { 7, "user1@mail.ru", 2, 7 },
                    { 8, "user2@mail.ru", 2, 8 },
                    { 9, "user3@mail.ru", 2, 9 },
                    { 10, "user4@mail.ru", 2, 10 },
                    { 12, "@dev1", 1, 4 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DepartmentId", "FirstName", "HeadDepartmentId", "LastName", "Login", "Password", "PatronymicName", "Position", "RoleId" },
                values: new object[,]
                {
                    { 5, 5, "Виктория", 5, "Федорова", "dev2", null, "Александровна", "Руководитель отдела", 3 },
                    { 6, 5, "Денис", null, "Лебедев", "dev3", null, "Иванович", "разработчик2", 3 }
                });

            migrationBuilder.InsertData(
                table: "Vacations",
                columns: new[] { "Id", "Comment", "IsAccepted", "UserId", "VacationEndDate", "VacationStartDate", "VacationTypeId" },
                values: new object[,]
                {
                    { 1, null, true, 1, new DateTime(2023, 2, 20, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 2, 7, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 2, null, true, 1, new DateTime(2023, 7, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 7, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 3, null, true, 2, new DateTime(2023, 4, 28, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 4, 11, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 4, null, true, 2, new DateTime(2023, 6, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 6, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 5, null, true, 3, new DateTime(2023, 7, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 7, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 6, null, true, 3, new DateTime(2023, 10, 28, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 10, 15, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 7, null, true, 4, new DateTime(2023, 8, 19, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 8, null, true, 4, new DateTime(2023, 11, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 11, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 13, null, true, 7, new DateTime(2023, 1, 29, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 1, 15, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 14, null, true, 7, new DateTime(2023, 8, 19, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 15, null, true, 8, new DateTime(2023, 2, 20, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 2, 7, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 16, null, true, 8, new DateTime(2023, 7, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 7, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 17, null, true, 9, new DateTime(2023, 4, 24, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 4, 11, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 18, null, true, 9, new DateTime(2023, 6, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 6, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 19, null, true, 10, new DateTime(2023, 9, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 9, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 20, null, true, 10, new DateTime(2023, 12, 29, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 12, 15, 0, 0, 0, 0, DateTimeKind.Local), 1 }
                });

            migrationBuilder.InsertData(
                table: "UserContacts",
                columns: new[] { "Id", "Contact", "TypeContactId", "UserId" },
                values: new object[,]
                {
                    { 5, "dev2@mail.ru", 2, 5 },
                    { 6, "dev3@mail.ru", 2, 6 },
                    { 13, "@dev2", 1, 5 },
                    { 14, "@dev3", 1, 6 }
                });

            migrationBuilder.InsertData(
                table: "Vacations",
                columns: new[] { "Id", "Comment", "IsAccepted", "UserId", "VacationEndDate", "VacationStartDate", "VacationTypeId" },
                values: new object[,]
                {
                    { 9, null, true, 5, new DateTime(2023, 4, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 4, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 10, null, true, 5, new DateTime(2023, 7, 28, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 7, 11, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 11, null, true, 6, new DateTime(2023, 2, 20, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 2, 7, 0, 0, 0, 0, DateTimeKind.Local), 1 },
                    { 12, null, true, 6, new DateTime(2023, 7, 14, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2023, 7, 1, 0, 0, 0, 0, DateTimeKind.Local), 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Departments_ParentId",
                table: "Departments",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserContacts_TypeContactId",
                table: "UserContacts",
                column: "TypeContactId");

            migrationBuilder.CreateIndex(
                name: "IX_UserContacts_UserId",
                table: "UserContacts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DepartmentId",
                table: "Users",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_HeadDepartmentId",
                table: "Users",
                column: "HeadDepartmentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Vacations_UserId",
                table: "Vacations",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vacations_VacationTypeId",
                table: "Vacations",
                column: "VacationTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "UserContacts");

            migrationBuilder.DropTable(
                name: "Vacations");

            migrationBuilder.DropTable(
                name: "ContactTypes");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "VacationTypes");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Role");
        }
    }
}
